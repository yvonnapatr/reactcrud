import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router, Route, Switch, Redirect
} from 'react-router-dom';

import store from './store/createStore';

import Footer from './modules/admin/Footer';
import Header from './modules/admin/Header';
import Users from './modules/admin/Users';
import NewUser from './modules/admin/NewUser';
import EditUser from './modules/admin/EditUser';


function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <div className="hero is-fullheight-with-navbar is-light">
          <div className="hero-body">
            <div className="container">
              <Router>
                <Switch>
                  <Redirect exact from="/" to="/admin/users" />
                  <Route exact path="/admin/users">
                    <Users />
                  </Route>
                   <Route path="/admin/users/new" component = {NewUser} />
                  <Route path="/admin/users/:id" component={EditUser} /> 
                </Switch>
              </Router>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    </Provider>
  );
}

export default App;