import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import User from './User';
import { fetchUsers, removeUser, filterUsers } from '../../actions/userActions';
import SearchUsers from './SearchUsers';

class Users extends Component {

    constructor(props) {
        super(props);
        this.state = {
            titles: ['#', 'Nombre', 'Apellidos','Role' , 'Email', 'Telefono', 'Acciones']
        };
    }
    async componentDidMount() {
     
        this.props.getUsers();
    }

    handleDelete = id => {
        this.props.removeUser(id);
    };

    handleSearchUsers = searchValue => {
        this.props.filterUsers (searchValue);
    }


    render() {
        const { titles } = this.state;
        const users = this.props.users;
        const filtered = this.props.filtered.length ? this.props.filtered : users;

        return (

            <div className="column is-10 is-offset-1">
            <div className="level">
                <div className="level-left">
                    <h2 className="subtitle is-3">
                        Lista de Usuarios
                    </h2>
                </div>
                <div className="level-right">
                        <Link to="/admin/users/new" className="button is-link">Nuevo Usuario</Link>
                </div>
            </div>
        
       
            {filtered.length ? (
                  <>
              <SearchUsers  handleSearchUsers = {this.handleSearchUsers}/>
                    <table className="table is-striped is-hoverable is-bordered is-fullwidth">
                        <thead>
                            <tr>
                                {titles.map((title, idx) => (
                                    <th key={idx}>{title}</th>
                                ))}
                            </tr>
                        </thead>
                        <tbody>
                            {filtered.map((user, idx) => (
                                <User
                                    user={user}
                                    key={user.id}
                                    index={idx}
                                    handleDelete={this.handleDelete}
                                />
                            ))}
                        </tbody>
                    </table>
                    </>
            ) : <p className="has-text-centered">No hay Usuarios, genera uno.</p>}
        </div>



        );
    }

}

const mapStateToProps = state => {
    return {
        users: state.users.items,
        filtered: state.users.filtered
    }
}


const mapDispatchToProps = dispatch => {
    return {
        getUsers: () => {
            dispatch(fetchUsers());
        },
        removeUser: (id) => {
            dispatch(removeUser(id));
        },
        filterUsers :  (searchValue) => {
            dispatch(filterUsers(searchValue))
        }
    };
};

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(Users)
);