import React from 'react'

const Header = () => {
    return (
        <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <a className="navbar-item" href="/admin/users">
                    <img src={process.env.PUBLIC_URL + '/logo192.png'} alt="logo" />dmin
                </a>

                {/* <a role="button" className="navbar-burger burger" aria-label="menu" 
                aria-expanded="false" data-target="navbarBasicExample">
                    
                </a> */}
            </div>

            <div id="navbarBasicExample" className="navbar-menu">
                <div className="navbar-end">
                    <div className="navbar-item">
                        <span style={{ margin: '0 1.3em' }}>
                            Juan Perez
                        </span>
                        <div className="buttons">
                            <button className="button is-small is-info" type="button">
                                <span>
                                    Cerrar Sesión
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default Header;
